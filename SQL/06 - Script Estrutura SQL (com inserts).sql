CREATE DATABASE  IF NOT EXISTS `projeto_interdisciplinar` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `projeto_interdisciplinar`;

DROP TABLE IF EXISTS `atividade`;
CREATE TABLE `atividade` (
  `id_atividade` int NOT NULL AUTO_INCREMENT,
  `id_aula` int NOT NULL,
  `matricula_usuario` bigint NOT NULL,
  `data_cadastro` date NOT NULL,
  `data_final` date NOT NULL,
  `titulo_atividade` varchar(255) NOT NULL,
  `descricao_atividade` varchar(1028) NOT NULL,
  `fontes_atividade` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id_atividade`),
  KEY `fk_matricula_usuario_idx` (`matricula_usuario`),
  KEY `fk_atividade_aula_idx` (`id_aula`),
  CONSTRAINT `fk_atividade_aula` FOREIGN KEY (`id_aula`) REFERENCES `aula` (`id_aula`),
  CONSTRAINT `fk_atividade_usuario` FOREIGN KEY (`matricula_usuario`) REFERENCES `usuario` (`matricula_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `atividade` WRITE;
INSERT INTO `atividade` VALUES (32,2,1050481913019,'2020-04-16','2020-04-14','Teste1','sijsaidd asdfjs','soidjsa'),(33,2,1050481913019,'2020-04-16','2020-04-15','asdfas','asdfsadfsad','asdfdsaf'),(35,47,1050481913019,'2020-04-16','2020-04-25','Atividade 01','Preencher formulário disponível no site da professora.','www.mpc.com.br'),(36,47,1050481913019,'2020-04-16','2020-04-10','Avaliação 01','Conteúdo até o dia 20/03','www.mpc.com.br, www.estudos-prova.com'),(37,47,1050481913019,'2020-04-16','2020-04-18','Prova Substitutiva 01','Mesmo conteúdo da primeira avaliação.','www.mpc.com.br, www.estudos-prova.com'),(38,1,1050481913019,'2020-04-16','2020-04-24','Entrega parcial do PI','Entregar os 3 primeiros itens pedidos pelo professor conforme link.','www.pi-arq-comp.com.br');
UNLOCK TABLES;


DROP TABLE IF EXISTS `aula`;
CREATE TABLE `aula` (
  `id_aula` int NOT NULL AUTO_INCREMENT,
  `id_curso` int NOT NULL,
  `id_materia` int NOT NULL,
  `semestre` int NOT NULL,
  PRIMARY KEY (`id_aula`),
  KEY `fk_aula_curso_idx` (`id_curso`),
  KEY `fk_aula_materia_idx` (`id_materia`),
  CONSTRAINT `fk_aula_curso` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id_curso`),
  CONSTRAINT `fk_aula_materia` FOREIGN KEY (`id_materia`) REFERENCES `materia` (`id_materia`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `aula` WRITE;
INSERT INTO `aula` VALUES (1,1,1,1),(2,1,2,1),(3,1,3,1),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,7,1),(8,1,8,2),(9,1,9,2),(10,1,10,2),(11,1,11,2),(12,1,12,2),(13,1,13,2),(14,1,14,2),(15,1,15,3),(16,1,16,3),(17,1,17,3),(18,1,18,3),(19,1,19,3),(20,1,20,3),(21,1,21,4),(22,1,22,4),(23,1,23,4),(24,1,24,4),(25,1,25,4),(26,1,26,4),(27,1,27,4),(28,1,28,4),(29,1,29,5),(30,1,30,5),(31,1,31,5),(32,1,32,5),(33,1,33,5),(34,1,34,5),(35,1,35,5),(36,1,36,6),(37,1,37,6),(38,1,38,6),(39,1,39,6),(40,1,40,6),(41,1,41,6),(42,1,42,6),(43,1,43,6),(44,2,20,1),(45,2,21,1),(46,2,22,1),(47,2,23,1),(48,2,24,2),(49,2,25,2),(50,2,26,2),(51,2,27,2),(52,2,28,3),(53,2,29,3),(54,2,30,3),(55,2,31,3),(56,2,31,4),(57,2,32,4),(58,2,33,4),(59,2,34,4),(60,2,35,5),(61,2,36,5),(62,2,37,5),(63,2,38,5),(64,2,39,6),(65,2,40,6),(66,2,41,6),(67,2,42,6),(68,3,43,1),(69,3,44,1),(70,3,45,1),(71,3,46,1),(72,3,47,2),(73,3,48,2),(74,3,49,2),(75,3,50,2),(76,3,51,3),(77,3,52,3),(78,3,53,3),(79,3,54,3),(80,3,55,4),(81,3,56,4),(82,3,57,4),(83,3,58,4),(84,3,59,5),(85,3,60,5),(86,3,61,5),(87,3,62,5),(88,3,63,6),(89,3,64,6),(90,3,65,6),(91,3,66,6),(92,4,67,1),(93,4,68,1),(94,4,69,1),(95,4,70,1),(96,4,71,2),(97,4,72,2),(98,4,73,2),(99,4,74,2),(100,4,75,3),(101,4,76,3),(102,4,77,3),(103,4,78,3),(104,4,79,4),(105,4,80,4),(106,4,81,4),(107,4,82,4),(108,4,83,5),(109,4,84,5),(110,4,85,5),(111,4,86,5),(112,4,87,6),(113,4,88,6),(114,4,89,6),(115,4,90,6),(116,5,43,1),(117,5,44,1),(118,5,45,1),(119,5,46,1),(120,5,47,2),(121,5,48,2),(122,5,49,2),(123,5,50,2),(124,5,51,3),(125,5,52,3),(126,5,53,3),(127,5,54,3),(128,5,55,4),(129,5,56,4),(130,5,57,4),(131,5,58,4),(132,5,59,5),(133,5,60,5),(134,5,61,5),(135,5,62,5),(136,5,63,6),(137,5,64,6),(138,5,65,6),(139,5,66,6),(140,6,67,1),(141,6,68,1),(142,6,69,1),(143,6,70,1),(144,6,71,2),(145,6,72,2),(146,6,73,2),(147,6,74,2),(148,6,75,3),(149,6,76,3),(150,6,77,3),(151,6,78,3),(152,6,79,4),(153,6,80,4),(154,6,81,4),(155,6,82,4),(156,6,83,5),(157,6,84,5),(158,6,85,5),(159,6,86,5),(160,6,87,6),(161,6,88,6),(162,6,89,6),(163,6,90,6),(164,7,43,1),(165,7,44,1),(166,7,45,1),(167,7,46,1),(168,7,47,2),(169,7,48,2),(170,7,49,2),(171,7,50,2),(172,7,51,3),(173,7,52,3),(174,7,53,3),(175,7,54,3),(176,7,55,4),(177,7,56,4),(178,7,57,4),(179,7,58,4),(180,7,59,5),(181,7,60,5),(182,7,61,5),(183,7,62,5),(184,7,63,6),(185,7,64,6),(186,7,65,6),(187,7,66,6);
UNLOCK TABLES;


DROP TABLE IF EXISTS `aux_usuario_aula`;
CREATE TABLE `aux_usuario_aula` (
  `id_vinculacao` int NOT NULL AUTO_INCREMENT,
  `matricula_usuario` bigint NOT NULL,
  `id_aula` int NOT NULL,
  PRIMARY KEY (`id_vinculacao`),
  KEY `fk_aux_usuario_idx` (`matricula_usuario`),
  KEY `fk_aux_materia_idx` (`id_aula`),
  CONSTRAINT `fk_aux_aula` FOREIGN KEY (`id_aula`) REFERENCES `aula` (`id_aula`),
  CONSTRAINT `fk_aux_usuario` FOREIGN KEY (`matricula_usuario`) REFERENCES `usuario` (`matricula_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `aux_usuario_aula` WRITE;
INSERT INTO `aux_usuario_aula` VALUES (85,1050481913019,1),(86,1050481913019,2),(87,1050481913019,47);
UNLOCK TABLES;


DROP TABLE IF EXISTS `curso`;
CREATE TABLE `curso` (
  `id_curso` int NOT NULL AUTO_INCREMENT,
  `nome_curso` varchar(255) NOT NULL,
  PRIMARY KEY (`id_curso`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `curso` WRITE;
INSERT INTO `curso` VALUES (1,'Análise e Desenv. de Sistemas'),(2,'Comércio Exterior'),(3,'Gestão Empresarial (Vespertino)'),(4,'Gestão de Serviços'),(5,'Logística Aeroportuária - Falta'),(6,'Redes de Computadores - Falta'),(7,'Gestão Empresarial (Noturno) - Falta');
UNLOCK TABLES;


DROP TABLE IF EXISTS `materia`;
CREATE TABLE `materia` (
  `id_materia` int NOT NULL AUTO_INCREMENT,
  `nome_materia` varchar(255) NOT NULL,
  PRIMARY KEY (`id_materia`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `materia` WRITE;
INSERT INTO `materia` VALUES (1,'Arq. Comp.'),(2,'Matemática Discreta'),(3,'Algoritmos'),(4,'Inglês I'),(5,'Prog. Microinformática'),(6,'Lab. Hard.'),(7,'Adm. Geral'),(8,'Contabilidade'),(9,'Eng. Software I'),(10,'Com. Exp.'),(11,'Inglês II'),(12,'Sist. Inform.'),(13,'Calculo'),(14,'Ling. Prod. I'),(15,'Inglês III'),(16,'G.G.T.I'),(17,'P.O.O'),(18,'Banco de dados'),(19,'Economia e Finanças'),(20,'Eng. Software II'),(21,'Estatística'),(22,'S.O I'),(23,'M.P.C'),(24,'Estr. Dados'),(25,'I.H.C'),(26,'Soc. Tec.'),(27,'Inglês IV'),(28,'Prog. Web'),(29,'G. Projetos'),(30,'S.O II'),(31,'Inglês V'),(32,'Lab. Banco de Dados'),(33,'Eng. Software III'),(34,'Seg. Informação'),(35,'Prog. Linear'),(36,'Prog. Disp. Móveis'),(37,'G. Equipes'),(38,'Top. Espec. em Informática'),(39,'Empreend.'),(40,'Inglês VI'),(41,'Ética'),(42,'Aud. Sistemas'),(43,'Redes de Computadores'),(44,'Lab. Eng. Software'),(45,'Matemática Aplicada'),(46,'Comércio Exterior'),(47,'D.P.P.'),(48,'Inglês I e II'),(49,'Com. Exp. I'),(50,'Inglês III e IV'),(51,'Economia'),(52,'Sist. Inform. Contáveis'),(53,'Política Externa'),(54,'Estatística Aplic.'),(55,'Dir. Internacion'),(56,'Proj. Comex. I'),(57,'Com. Exp. II'),(58,'Economia Internacional'),(59,'Proj. Comex. II'),(60,'Log. Aplicada.'),(61,'Inglês V e VI'),(62,'G. Finanças'),(63,'Espanhol I'),(64,'G.C.T.'),(65,'Log. Intern.'),(66,'Mercado Financ. Intern.'),(67,'Inglês VII e VIII'),(68,'Proj. Comex. III'),(69,'Sist. Comex'),(70,'Log. Intern.'),(71,'Espanhol II'),(72,'Legislação Aduaneira'),(73,'Teoria Prat. Cambial'),(74,'Mkt.'),(75,'G. Pessoas'),(76,'Inglês IX e X'),(77,'Proj. Comex. IV'),(78,'Reg. Aduan. Especiais'),(79,'Espanhol III'),(80,'Espanhol IV'),(81,'Gest. Estrat. Intern.'),(82,'Inglês XI'),(83,'Mkt. Internacional'),(84,'Modais Transp. e Seg. Cargas'),(85,'Neg. Intern.'),(86,'Proj. Comex. V'),(87,'Com. Exp.'),(88,'Matemática'),(89,'Soc. Tec.'),(90,'Info. AAP'),(91,'Inform. Apl. Gestão'),(92,'Comp. Org.'),(93,'Economia'),(94,'Comp. Org. AAP'),(95,'Sociol. Organiz.'),(96,'Estatística'),(97,'G. Amb.'),(98,'Sist. Info.'),(99,'G. Mkt.'),(100,'Osm. AAP'),(101,'O.S.M.'),(102,'P. Mkt.'),(103,'P. Mkt. - AAP'),(104,'G. Financ.'),(105,'Com. Empr.'),(106,'Logística'),(107,'Direito Empresarial'),(108,'P.T.G I'),(109,'Fund. Gest. Qualidade'),(110,'Anal. Invest.'),(111,'G. Proj. - AAP'),(112,'Espanhol I'),(113,'Gest. Proj.'),(114,'G. Produção'),(115,'Sist. Int. Gestão'),(116,'Plan. Gest. Estratégica'),(117,'Espanhol II'),(118,'Desenv. Negoc.'),(119,'Neg. Intern.'),(120,'Des. Neg. - AAP'),(121,'Intro. Merc. Serviços'),(122,'Fund. Gest. Eventos'),(123,'Proj. Integ. I'),(124,'Fund. Inform.'),(125,'Proj. Integ. II'),(126,'Fund. Custos Preços e Orç.'),(127,'Fund. Gestão Conhecimento'),(128,'Marketing Serviços'),(129,'Comport. Organiz.'),(130,'Fund. Gestão Financeira'),(131,'Engenharia de Desenv. Serviços'),(132,'Proj. Integ. III'),(133,'Fund. Anal. Gest. Riscos'),(134,'Técnicas de Negociação'),(135,'Inteligência de Negócios'),(136,'Modelagem simul. proc. e serviços'),(137,'Proj. Integ. IV'),(138,'Gest. Negócios e Serviços'),(139,'Projetos Ambientes Físico'),(140,'Fund. Análise de Proj. Invest.'),(141,'Gest. de Tecn. Informação'),(142,'Proj. Integ. V'),(143,'Anal. Gest. Qualid. Serv.'),(144,'Gest. Operações de Serviços'),(145,'Serv. Emp. Intens. Conhec.'),(146,'Serv. Emp. Terceiro Setor'),(147,'Serv. Emp. Setor Público'),(148,'Serv. Emp. Lazer e Turismo'),(149,'Serv. Emp. Saúde'),(150,'Serv. Emp. Varejo'),(151,'Proj. Integ. VI');
UNLOCK TABLES;


DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `matricula_usuario` bigint NOT NULL,
  `id_curso` int NOT NULL,
  `senha_usuario` varchar(512) NOT NULL,
  `nome_usuario` varchar(255) NOT NULL,
  `email_usuario` varchar(255) NOT NULL,
  `matricula_responsavel` bigint DEFAULT NULL,
  PRIMARY KEY (`matricula_usuario`),
  KEY `fk_matricula_responsavel_idx` (`matricula_responsavel`),
  KEY `fk_usuario_curso_idx` (`id_curso`),
  CONSTRAINT `fk_usuario_curso` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id_curso`),
  CONSTRAINT `fk_usuario_responsavel` FOREIGN KEY (`matricula_responsavel`) REFERENCES `usuario` (`matricula_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `usuario` WRITE;
INSERT INTO `usuario` VALUES (1,1,'03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4','Administrador','admin.admin@fatec.sp.gov.br',NULL),(1050481913019,1,'03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4','Guilherme Clemente','guilherme.clemente@fatec.sp.gov.br',1);
UNLOCK TABLES;